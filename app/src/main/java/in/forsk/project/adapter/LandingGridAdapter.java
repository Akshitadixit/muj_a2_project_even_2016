package in.forsk.project.adapter;

import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import java.util.ArrayList;

import in.forsk.project.R;
import in.forsk.project.wrapper.LandingGridWrapper;

/**
 * Created by Saurabh on 2/20/2016.
 */
public class LandingGridAdapter extends RecyclerView.Adapter<LandingGridAdapter.ViewHolder> {

    private final static String TAG = LandingGridAdapter.class.getSimpleName();
    private ArrayList<LandingGridWrapper> mObjList;

    OnItemClicklistner onItemClicklistner;

    public LandingGridAdapter(ArrayList<LandingGridWrapper> mObjList) {
        this.mObjList = mObjList;
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View v = LayoutInflater.from(parent.getContext()).inflate(R.layout.landing_grid_cell, parent, false);
        return new ViewHolder(v);
    }

    @Override
    public void onBindViewHolder(ViewHolder holder, int position) {
        LandingGridWrapper mObj = mObjList.get(position);

        holder.textView.setText(mObj.title);
        holder.imageView.setImageResource(mObj.drawable);
//        holder.imageView.setText(mObj.id);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public int getItemCount() {
        return mObjList.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener {
        ImageView imageView;
        TextView textView;

        public ViewHolder(View v) {
            super(v);
            imageView = (ImageView) v.findViewById(R.id.imageView);
            textView = (TextView) v.findViewById(R.id.textView);
            v.setOnClickListener(this);
        }

        @Override
        public void onClick(View v) {
            if(onItemClicklistner != null){
                onItemClicklistner.onItemClick(v,getPosition());
            }
        }
    }

    public interface  OnItemClicklistner{
        public void onItemClick(View v,int position);
    }

    public void setOnItemClicklistner(OnItemClicklistner onItemClicklistner){
        this.onItemClicklistner = onItemClicklistner;
    }
}
